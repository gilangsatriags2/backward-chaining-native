<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <title>Sikar Softlens</title>
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="images/favicon.png">
    <!-- Custom Stylesheet -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-rbsA2VBKQhggwzxH7pPCaAqO46MgnOM80zW1RWuH61DGLwZJEdK2Kadq2F9CUG65" crossorigin="anonymous">
    <link href="css/style.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.8.3/font/bootstrap-icons.css">
    <link rel="stylesheet" href="//cdn.datatables.net/1.13.1/css/jquery.dataTables.min.css">
</head>
<style>
    .container-edit-main {
        width: 400px;
        border: 1px none black;
        width: 500px;
        border-radius: 13px;
        padding: 40px;
        overflow: hidden;
        box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
    }

    .con-button {
        justify-content: space-between;
    }
</style>
<body>

    <!--*******************
        Preloader start
    ********************-->
    <div id="preloader">
        <div class="loader">
            <svg class="circular" viewBox="25 25 50 50">
                <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="3" stroke-miterlimit="10" />
            </svg>
        </div>
    </div>
    <!--*******************
        Preloader end
    ********************-->

    
    <!--**********************************
        Main wrapper start
    ***********************************-->
    <div id="main-wrapper">

        <!--**********************************
            Nav header start
        ***********************************-->
        <div class="nav-header">
            <div class="brand-logo">
                <a href="#">
                    <b class="logo-abbr"><img src="asset/softlens.png" alt="" style="width:30px; height:auto;"> </b>
                    <span class="logo-compact"><img src="asset/softlens.png" style="width:30px; height:auto;" alt=""></span>
                    <span class="brand-title">
                        <img src="asset/softlens.png" style="width:30px; height:auto;" alt=""><span style="color: white; font-size: 22px;"> Sikar Softlens</span>
                    </span>
                </a>
            </div>
        </div>
        <!--**********************************
            Nav header end
        ***********************************-->

        <!--**********************************
            Header start
        ***********************************-->
        <div class="header">    
            <div class="header-content clearfix" style="font-size: 25px;">
                <div class="nav-control">
                    <div class="hamburger">
                        <span class="toggle-icon"><i class="icon-menu"></i></span>
                    </div>
                    Gejala
                </div>
            </div>
        </div>
        <!--**********************************
            Header end ti-comment-alt
        ***********************************-->

        <!--**********************************
            Sidebar start
        ***********************************-->
        <div class="nk-sidebar">           
            <div class="nk-nav-scroll">
                <ul class="metismenu" id="menu">
                    <li class="nav-label">Dashboard</li>
                    <li class="mega-menu mega-menu-sm">
                        <a class="has-arrow" href="index_gejala.php" aria-expanded="false" style="text-decoration:none;">
                            <i class="icon-speedometer menu-icon"></i><span class="nav-text">Gejala</span>
                        </a>
                    </li>
                    <li class="mega-menu mega-menu-sm">
                        <a class="has-arrow" href="index_penyakit.php" aria-expanded="false" style="text-decoration:none;">
                            <i class="icon-globe-alt menu-icon"></i><span class="nav-text">Penyakit</span>
                        </a>
                    </li>
                    <li class="mega-menu mega-menu-sm">
                        <a class="has-arrow" href="index_pengetahuan.php" aria-expanded="false" style="text-decoration:none;">
                            <i class="icon-globe-alt menu-icon"></i><span class="nav-text">Basis Pengetahuan</span>
                        </a>
                    </li>
                    <li class="mega-menu mega-menu-sm">
                        <a class="has-arrow" href="logout.php" aria-expanded="false" style="text-decoration:none;">
                            <i class="icon-globe-alt menu-icon"></i><span class="nav-text">Keluar</span>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
        <!--**********************************
            Sidebar end
        ***********************************-->

        <!--**********************************
            Content body start
        ***********************************-->
        <div class="content-body" >
            <div class="row page-titles mx-0">
                <div class="col p-md-0">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="javascript:void(0)">Dashboard</a></li>
                        <li class="breadcrumb-item active"><a href="javascript:void(0)">Gejala</a></li>
                    </ol>
                </div>
            </div>
            <!-- row -->
            
            <div class="container-fluid">
                    <?php
                    include "connection.php";
                    $id_gejala = $_GET['id_gejala'];
                    $sql = "SELECT * FROM gejala WHERE id_gejala = '$id_gejala'";

                    //queri ke MySQL
                    $result = mysqli_query($mysqli, $sql);
                    $r = mysqli_fetch_assoc($result);
                    ?>
                    <h3 class="text-center">Edit Gejala</h3>
                    <div class="container-edit mx-auto">
                        <form method="post" action="edit_gejala.php">
                            <input type="hidden" name="id_gejala" value="<?php echo $r['id_gejala']; ?>">
                            <hr><br>
                            <label for="">Kode Gejala</label>
                            <div class="input-group mb-3">
                                <span class="input-group-text" id="basic-addon1"><i class="bi bi-123"></i></span>
                                <input type="text" class="form-control" value="<?php echo $r['kode_gejala']; ?>" name="kode_gejala"
                                    id="exampleInputNpm" aria-describedby="npmHelp" required>
                            </div>
                            <label for="">Nama Gejala</label>
                            <div class="input-group mb-3">
                                <span class="input-group-text" id="basic-addon1"><i class="bi bi-asterisk"></i></span>
                                <input type="text" class="form-control" value="<?php echo $r['nama_gejala']; ?>" name="nama_gejala"
                                    id="exampleInputNama" required>
                            </div>
                            <label for="">Pertanyaan</label>
                            <div class="input-group mb-3">
                                <span class="input-group-text" id="basic-addon1"><i class="bi bi-journal-check"></i></span>
                                <input type="text" class="form-control" value="<?php echo $r['pertanyaan']; ?>" name="pertanyaan"
                                    id="exampleInputNama" required>
                            </div>
                            <div class="con-button d-flex">
                                <a href="index_gejala.php" type="button" class="btn btn-danger text-center">Batal</a>
                                <button type="submit" class="btn btn-primary text-center">Submit</button>
                            </div>
                        </form>
                    </div>
            </div>
            <!-- #/ container -->
        </div>
        <!--**********************************
            Content body end
        ***********************************-->
        
        
        <!--**********************************
            Footer start
        ***********************************-->
        <div class="footer">
            <div class="copyright">
                <p>Dibuat dengan sepenuh hati <i class="bi bi-heart-fill" style="color: red;"></i> Nahee</p>
            </div>
        </div>
        <!--**********************************
            Footer end
        ***********************************-->
    </div>
    <!--**********************************
        Main wrapper end
    ***********************************-->

    <!--**********************************
        Scripts
    ***********************************-->
    <script src="plugins/common/common.min.js"></script>
    <script src="js/custom.min.js"></script>
    <script src="js/settings.js"></script>
    <script src="js/gleek.js"></script>
    <script src="js/styleSwitcher.js"></script>
    <script src="https://code.jquery.com/jquery-3.6.3.js" integrity="sha256-nQLuAZGRRcILA+6dMBOvcRh5Pe310sBpanc6+QBmyVM=" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/js/bootstrap.min.js" integrity="sha384-cuYeSxntonz0PPNlHhBs68uyIAVpIIOZZ5JqeqvYYIcEL727kskC66kF92t6Xl2V" crossorigin="anonymous"></script>
    <script src="//cdn.datatables.net/1.13.1/js/jquery.dataTables.min.js"></script>
</body>

</html>