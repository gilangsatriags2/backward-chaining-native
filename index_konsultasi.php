<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-rbsA2VBKQhggwzxH7pPCaAqO46MgnOM80zW1RWuH61DGLwZJEdK2Kadq2F9CUG65" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.8.3/font/bootstrap-icons.css">
    <link rel="stylesheet" href="asset/style.css">
    <title>Konsultasi</title>
</head>


<body>

    <div id="main">
    <a href="index.php" type="button" class="btn btn-danger btn-sm">Kembali</a>
        <h2 class="text-center"><span style="font-size:30px;cursor:pointer"></span>
            KONSULTASI </h2><br>
        <div class="container d-flex">
            <div class="mx-auto">
            <?php

            require "connection.php";

            $sql = "SELECT * FROM penyakit";
            $result = mysqli_query($mysqli, $sql); ?>
            <div class="card" style="width: 18rem;">
                <div class="card-body">
                    <form action="pilih_penyakit.php" method="post">
                        <h5 class="card-title">Pilih Penyakit</h5>
                        <select name="id_penyakit" class="form-select form-select-sm" aria-label=".form-select-sm example" required>
                            <option value="" hidden selected>Pilih</option>
                            <?php while ($r = mysqli_fetch_array($result)) { ?>
                                <option value="<?php echo $r['id_penyakit'] ?>">
                                    <?= $r['nama_penyakit'] ?>
                                </option>
                            <?php
                            }
                            ?>
                        </select><br>
                        <button type="submit" class="btn btn-primary btn-sm" name="submit" value="submit">Submit</button>
                    </form>
                </div>
            </div>
            </div>
        </div>


    </div>

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/js/bootstrap.min.js" integrity="sha384-cuYeSxntonz0PPNlHhBs68uyIAVpIIOZZ5JqeqvYYIcEL727kskC66kF92t6Xl2V" crossorigin="anonymous"></script>
</body>


</html>