<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-rbsA2VBKQhggwzxH7pPCaAqO46MgnOM80zW1RWuH61DGLwZJEdK2Kadq2F9CUG65" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/js/bootstrap.min.js"
        integrity="sha384-cuYeSxntonz0PPNlHhBs68uyIAVpIIOZZ5JqeqvYYIcEL727kskC66kF92t6Xl2V"
        crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.8.3/font/bootstrap-icons.css">
    <link rel="stylesheet" href="asset/style.css">
    <title>Konsultasi</title>
</head>

<body>

    <div id="main">
        <h2 class="text-center"><span style="font-size:30px;cursor:pointer" onclick="openNav()"></span>
            KONSULTASI </h2><br>
            <div class="d-flex">
                <div class="mx-auto">
        <?php
        require "connection.php";
        session_start();
        $id_penyakit = $_POST['id_penyakit'];
        $_SESSION['id'] = $id_penyakit;

        $sqlp = "SELECT nama_penyakit FROM penyakit WHERE id_penyakit = $id_penyakit";
        $nama_penyakit = mysqli_query($mysqli, $sqlp);
        $x = mysqli_fetch_array($nama_penyakit);

        $sql = "SELECT * FROM penyakit";
        $result = mysqli_query($mysqli, $sql);
        while ($r = mysqli_fetch_array($result)) {
            if ($id_penyakit == $r['id_penyakit']) {
                $sql2 = "SELECT p.pertanyaan, a.id_gejala AS id FROM gejala p, aturan a WHERE p.id_gejala = a.id_gejala AND a.id_penyakit = $id_penyakit";
                $result2 = mysqli_query($mysqli, $sql2); ?>
        <div class="card">
            <div class="card-body">
                <form action="hasil_konsultasi.php" method="post">
                    <div class="alert alert-success" role="alert">
                        <h5>Pertanyaan</h5>
                    </div>
                    <hr>
                    <?php
                while ($r2 = mysqli_fetch_array($result2)) { ?>
                    <div class="form-group">
                        <?php echo $r2['pertanyaan']; ?>
                        <div class="container-jawab" style="width: 5rem;">
                            <select name="jawab[]" class="form-select form-select-sm"
                                aria-label=".form-select-sm example" required>
                                <option value="" hidden selected>Pilih</option>
                                <option value="ya">Ya</option>
                                <option value="tidak">Tidak</option>
                            </select>
                        </div>
                    </div><br>
                    <?php
                }
            }
        }
                    ?>
                        <a href="index_konsultasi.php" class="btn btn-danger btn-sm">Kembali</a>
                    <button type="submit" class="btn btn-primary btn-sm" value="simpan" name="simpan">Submit</button>
                </form>
            </div>
        </div>
    </div>
        </div>
    </div>

    <script>
        function openNav() {
            document.getElementById("mySidenav").style.width = "250px";
            document.getElementById("main").style.marginLeft = "250px";
        }

        function closeNav() {
            document.getElementById("mySidenav").style.width = "0";
            document.getElementById("main").style.marginLeft = "0";
        }
    </script>
</body>

</html>