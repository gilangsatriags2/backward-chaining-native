<html>
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet"
    integrity="sha384-0evHe/X+R7YkIZDRvuzKMRqM+OrBnVFBL6DOitfPri4tjfHxaWutUpFmBp4vmVor" crossorigin="anonymous">
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.8.3/font/bootstrap-icons.css">
<style>
    .card {
        width: 50%;
    }
</style>

<div class="card mx-auto mt-5">
    <div class="card-header">
        Notice
    </div>
    <div class="card-body text-center">
        <?php
        include "connection.php";

        $kode_gejala = $_POST['kode_gejala'];
        $nama_gejala = $_POST['nama_gejala'];
        $id_gejala = $_POST['id_gejala'];
        $pertanyaan = $_POST['pertanyaan'];

        // QUERY UPDATE
        $sql = "UPDATE gejala SET kode_gejala='$kode_gejala', 
                    nama_gejala='$nama_gejala',
                    pertanyaan = '$pertanyaan'
                    WHERE id_gejala='$id_gejala'";

        // Fungsi untuk menginputkan hasil queri ke MySQL
        $result = mysqli_query($mysqli, $sql);
        if ($result) {
            header("Location: index_gejala.php");?>
        <?php
        } else {
            echo "Terjadi kesalahan"; ?>
        <a href="index_gejala.php" class="btn btn-primary btn-sm">Lihat Daftar Gejala</a>
        <?php
        } ?>

    </div>
</div>

<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/js/bootstrap.bundle.min.js"
    integrity="sha384-pprn3073KE6tl6bjs2QrFaJGz5/SUsLqktiwsUTF55Jfv3qYSDhgCecCxMW52nD2"
    crossorigin="anonymous"></script>

</html>