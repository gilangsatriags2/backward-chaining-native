<?php
session_start();

//cek apakah sudah login
if ($_SESSION['status'] != "sudah_login") {
    //melakukan pengalihan
    header("location:form_login.php");
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <title>Sikar Softlens</title>
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="images/favicon.png">
    <!-- Custom Stylesheet -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-rbsA2VBKQhggwzxH7pPCaAqO46MgnOM80zW1RWuH61DGLwZJEdK2Kadq2F9CUG65" crossorigin="anonymous">
    <link href="css/style.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.8.3/font/bootstrap-icons.css">
    <link rel="stylesheet" href="//cdn.datatables.net/1.13.1/css/jquery.dataTables.min.css">
</head>

<body>

    <!--*******************
        Preloader start
    ********************-->
    <div id="preloader">
        <div class="loader">
            <svg class="circular" viewBox="25 25 50 50">
                <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="3" stroke-miterlimit="10" />
            </svg>
        </div>
    </div>
    <!--*******************
        Preloader end
    ********************-->

    
    <!--**********************************
        Main wrapper start
    ***********************************-->
    <div id="main-wrapper">

        <!--**********************************
            Nav header start
        ***********************************-->
        <div class="nav-header">
            <div class="brand-logo">
                <a href="#">
                    <b class="logo-abbr"><img src="asset/softlens.png" alt="" style="width:30px; height:auto;"> </b>
                    <span class="logo-compact"><img src="asset/softlens.png" style="width:30px; height:auto;" alt=""></span>
                    <span class="brand-title">
                        <img src="asset/softlens.png" style="width:30px; height:auto;" alt=""><span style="color: white; font-size: 22px;"> Sikar Softlens</span>
                    </span>
                </a>
            </div>
        </div>
        <!--**********************************
            Nav header end
        ***********************************-->

        <!--**********************************
            Header start
        ***********************************-->
        <div class="header">    
            <div class="header-content clearfix" style="font-size: 25px;">
                <div class="nav-control">
                    <div class="hamburger">
                        <span class="toggle-icon"><i class="icon-menu"></i></span>
                    </div>
                    Pengetahuan
                </div>
            </div>
        </div>
        <!--**********************************
            Header end ti-comment-alt
        ***********************************-->

        <!--**********************************
            Sidebar start
        ***********************************-->
        <div class="nk-sidebar">           
            <div class="nk-nav-scroll">
                <ul class="metismenu" id="menu">
                    <li class="nav-label">Dashboard</li>
                    <li class="mega-menu mega-menu-sm">
                        <a class="has-arrow" href="index_gejala.php" aria-expanded="false" style="text-decoration:none;">
                            <i class="icon-speedometer menu-icon"></i><span class="nav-text">Gejala</span>
                        </a>
                    </li>
                    <li class="mega-menu mega-menu-sm">
                        <a class="has-arrow" href="index_penyakit.php" aria-expanded="false" style="text-decoration:none;">
                            <i class="icon-globe-alt menu-icon"></i><span class="nav-text">Penyakit</span>
                        </a>
                    </li>
                    <li class="mega-menu mega-menu-sm">
                        <a class="has-arrow" href="index_pengetahuan.php" aria-expanded="false" style="text-decoration:none;">
                            <i class="icon-globe-alt menu-icon"></i><span class="nav-text">Basis Pengetahuan</span>
                        </a>
                    </li>
                    <li class="mega-menu mega-menu-sm">
                        <a class="has-arrow" href="logout.php" aria-expanded="false" style="text-decoration:none;">
                            <i class="icon-globe-alt menu-icon"></i><span class="nav-text">Keluar</span>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
        <!--**********************************
            Sidebar end
        ***********************************-->

        <!--**********************************
            Content body start
        ***********************************-->
        <div class="content-body" >
            <div class="row page-titles mx-0">
                <div class="col p-md-0">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="javascript:void(0)">Dashboard</a></li>
                        <li class="breadcrumb-item active"><a href="javascript:void(0)">Pengetahuan</a></li>
                    </ol>
                </div>
            </div>
            <!-- row -->
            
            <div class="container-fluid">
            <button type="button" class="btn btn-success btn-sm" data-bs-toggle="modal" data-bs-target="#exampleModal" data-bs-whatever="@fat"><i class="bi bi-plus-lg"></i> Tambah
                    Pengetahuan</button>
                    <table class="table table-hover table-bordered" id="myTable" style="background-color: white;">
                <thead>
                    <tr>
                        <th width="50rem" ;>
                            No
                        </th>
                        <th width="150rem" ;>
                            Kode Aturan
                        </th>
                        <th width="300rem" ;>
                            Nama Gejala
                        </th>
                        <th>
                            Nama Penyakit
                        </th>
                        <th>
                            Aksi
                        </th>
                    </tr>
                </thead>
                <tbody>

                    <?php
                    include "connection.php";

                    $sql = "SELECT * FROM gejala, penyakit, aturan WHERE gejala.id_gejala = aturan.id_gejala AND penyakit.id_penyakit = aturan.id_penyakit ORDER BY kode_aturan ASC";
                    $result = mysqli_query($mysqli, $sql);

                    $i = 1;
                    while ($r = mysqli_fetch_assoc($result)) { ?>
                        <tr>
                            <td>
                                <?php echo $i; ?>
                            </td>
                            <td>
                                <?php echo $r['kode_aturan']; ?>
                            </td>
                            <td>
                                <?php echo $r['nama_gejala']; ?>
                            </td>
                            <td>
                                <?php echo $r['nama_penyakit']; ?>
                            </td>
                            <div class="d-flex justify-content-center text-center">
                                <td><a href="form_edit_aturan.php?id_aturan=<?php echo $r['id_aturan']; ?> " class="btn btn-warning btn-sm"><i class="bi bi-pencil"></i></a>
                                    <a href="hapus_aturan.php?id_aturan=<?php echo $r['id_aturan']; ?> " class="btn btn-danger btn-sm"><i class="bi bi-trash"></i></a>
                                </td>
                            </div>
                        </tr>
                    <?php $i++;
                    } ?>
                </tbody>
            </table>
            <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        <form method="post" action="tambah_aturan.php" class="form-tambah">
                            <div class="input bg bg-white text-dark">
                                <h3>Form Tambah Aturan</h3>
                            </div>
                            <hr>

                            <div class="input-group mb-3">
                                <span class="input-group-text" id="basic-addon1"><i class="bi bi-123"></i></span>
                                <input type="text" class="form-control" name="kode_aturan" id="exampleInputNpm" aria-describedby="npmHelp" placeholder="Kode Aturan" required>
                            </div>
                            <div class="input-group mb-3">
                            <span class="input-group-text" id="basic-addon1"><i class="bi bi-asterisk"></i></span>
                                <select name="id_gejala" class="form-select">
                                    <option value=" " selected hidden>Pilih Gejala</option>
                                    <?php
                                    $sql2 = "SELECT * FROM gejala";
                                    $result2 = mysqli_query($mysqli, $sql2);

                                    while ($r2 = mysqli_fetch_assoc($result2)) { ?>
                                        <option value="<?php echo $r2['id_gejala'] ?>">
                                            <?php echo $r2['nama_gejala'] ?>
                                        </option>
                                    <?php }

                                    ?>
                                </select>
                            </div>
                            <div class="input-group mb-3">
                            <span class="input-group-text" id="basic-addon1"><i class="bi bi-asterisk"></i></span>
                                <select name="id_penyakit" class="form-select">
                                    <option value=" " selected hidden >Pilih Penyakit</option>
                                    <?php
                                    $sql3 = "SELECT * FROM penyakit";
                                    $result3 = mysqli_query($mysqli, $sql3);

                                    while ($r3 = mysqli_fetch_assoc($result3)) { ?>
                                        <option value="<?php echo $r3['id_penyakit'] ?>">
                                            <?php echo $r3['nama_penyakit'] ?>
                                        </option>
                                    <?php }

                                    ?>
                                </select>
                            </div>
                            <div class="con-button d-flex">
                                <button type="submit" class="btn btn-primary text-center"><i class="bi bi-bookmark-plus"></i> Tambah</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
            </div>
            <!-- #/ container -->
        </div>
        <!--**********************************
            Content body end
        ***********************************-->
        
        
        <!--**********************************
            Footer start
        ***********************************-->
        <div class="footer">
            <div class="copyright">
                <p>Dibuat dengan sepenuh hati <i class="bi bi-heart-fill" style="color: red;"></i> Nahee</p>
            </div>
        </div>
        <!--**********************************
            Footer end
        ***********************************-->
    </div>
    <!--**********************************
        Main wrapper end
    ***********************************-->

    <!--**********************************
        Scripts
    ***********************************-->
    <script src="plugins/common/common.min.js"></script>
    <script src="js/custom.min.js"></script>
    <script src="js/settings.js"></script>
    <script src="js/gleek.js"></script>
    <script src="js/styleSwitcher.js"></script>
    <script src="https://code.jquery.com/jquery-3.6.3.js" integrity="sha256-nQLuAZGRRcILA+6dMBOvcRh5Pe310sBpanc6+QBmyVM=" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/js/bootstrap.min.js" integrity="sha384-cuYeSxntonz0PPNlHhBs68uyIAVpIIOZZ5JqeqvYYIcEL727kskC66kF92t6Xl2V" crossorigin="anonymous"></script>
    <script src="//cdn.datatables.net/1.13.1/js/jquery.dataTables.min.js"></script>
    <script>
        $(document).ready( function () {
            $('#myTable').DataTable();
        } );
    </script>
</body>

</html>