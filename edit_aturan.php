<html>
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet"
    integrity="sha384-0evHe/X+R7YkIZDRvuzKMRqM+OrBnVFBL6DOitfPri4tjfHxaWutUpFmBp4vmVor" crossorigin="anonymous">
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.8.3/font/bootstrap-icons.css">
<style>
    .card {
        width: 50%;
    }
</style>

<div class="card mx-auto mt-5">
    <div class="card-header">
        Notice
    </div>
    <div class="card-body text-center">
        <?php
        include "connection.php";

        $kode_aturan = $_POST['kode_aturan'];
        $id_gejala = $_POST['id_gejala'];
        $id_penyakit = $_POST['id_penyakit'];
        $id_aturan = $_POST['id_aturan'];

        // QUERY UPDATE
        $sql = "UPDATE aturan SET kode_aturan='$kode_aturan', 
                    id_gejala='$id_gejala',
                    id_penyakit = '$id_penyakit'
                    WHERE id_aturan='$id_aturan'";

        // Fungsi untuk menginputkan hasil queri ke MySQL
        $result = mysqli_query($mysqli, $sql);
        if ($result) {
            header('Location: index_pengetahuan.php') ?>
        <?php
        } else {
            echo "Terjadi kesalahan"; ?>
        <a href="index_pengetahuan.php" class="btn btn-primary btn-sm">Lihat Daftar Gejala</a>
        <?php
        } ?>

    </div>
</div>

<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/js/bootstrap.bundle.min.js"
    integrity="sha384-pprn3073KE6tl6bjs2QrFaJGz5/SUsLqktiwsUTF55Jfv3qYSDhgCecCxMW52nD2"
    crossorigin="anonymous"></script>

</html>

