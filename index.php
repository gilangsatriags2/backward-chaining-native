
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-0evHe/X+R7YkIZDRvuzKMRqM+OrBnVFBL6DOitfPri4tjfHxaWutUpFmBp4vmVor" crossorigin="anonymous">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Dancing+Script:wght@400;700&family=Poppins:wght@100;200;400;700&family=Roboto:wght@400;700&display=swap" rel="stylesheet">
    <style>
    *{
        padding: 0;
        margin: 0;
    }
    body{
        background-color: #2D31FA;
        background-repeat: no-repeat;
        font-family: 'Poppins', sans-serif;
    }
    .custom-shape-divider-bottom-1657895343 {
    position: absolute;
    bottom: 0;
    left: 0;
    width: 100%;
    overflow: hidden;
    line-height: 0;
    }

    .custom-shape-divider-bottom-1657895343 svg {
        position: relative;
        display: block;
        width: calc(140% + 1.3px);
        height: 180px;
    }

    .custom-shape-divider-bottom-1657895343 .shape-fill {
        fill: #FFFFFF;
    }
    .nav-item{
        font-size: 15px;
    }
    .btn{
        border-radius: 20px;
        font-size: 15px;
    }
    .col-4 img{
        width: 500px;
    }
    </style>
    <title>LOGIN</title>
</head>
<body>
    <div class="custom-shape-divider-bottom-1657895343">
        <svg data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1200 120" preserveAspectRatio="none">
            <path d="M985.66,92.83C906.67,72,823.78,31,743.84,14.19c-82.26-17.34-168.06-16.33-250.45.39-57.84,11.73-114,31.07-172,41.86A600.21,600.21,0,0,1,0,27.35V120H1200V95.8C1132.19,118.92,1055.71,111.31,985.66,92.83Z" class="shape-fill"></path>
        </svg>
    </div>

    <!-- navbar -->
    <?php if(isset($_GET['pesan'])){?>
                <div class="alert alert-danger alert-dismissible fade show mx-auto mt-2" role="alert">
                    <strong>
                        <label for="" class="text-center"><?php echo $_GET['pesan'];?></label>
                    </strong>
                    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                </div>
            <?php
            }
            ?>
    <nav class="navbar navbar-expand-lg bg-transparant ">
        <div class="container">
            <a class="navbar-brand text-light" href="index.php"><h2><img src="asset/softlens.png" style="width: 40px; height:auto;" alt=""> Sikar Softlens</h2></a>
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse justify-content-end" id="navbarNav">
                <ul class="navbar-nav">
                    <li class="nav-item">
                        <a class="nav-link active text-light mx-2" aria-current="page" href="index.php">Home</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link text-light mx-2" href="daftar_penyakit.php">Daftar Penyakit</a>
                    </li>
                    <li class="nav-item">
                        <a href="form_login.php" type="button" class="btn btn-success text-light">Login Admin/Pakar</a>
                    </li>
                </ul>
            </div>
        </div>
    </nav><br><br>

    <!-- jumbotron -->
    <div class="jumbotron">
        <div class="container-jumbotron">
            <div class="row justify-content-around text-light mt-5">
                <div class="col-4 mt-3">
                    <p class="lead"><h2>Sikar <span class="fw-bold">Softlens </span> <br>Sistem Pakar Online Berbasis Web</h2></p>
                    <p style="font-size: 13px;">Sikar Softlens merupakan web sistem pakar untuk mendiagnosa penyakit mata akibat dampak penggunaan Softlens.</p>
                    <a class="btn btn-primary btn-lg text-light" href="index_konsultasi.php" role="button">Konsultasi Sekarang</a>
                </div>
                <div class="col-4 mt-3">
                    <img src="asset/dokter.png" style="width: 150px; height: auto;"alt="">
                </div>
            </div>
        </div>
    </div>

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-pprn3073KE6tl6bjs2QrFaJGz5/SUsLqktiwsUTF55Jfv3qYSDhgCecCxMW52nD2" crossorigin="anonymous"></script>

</body>
</html>