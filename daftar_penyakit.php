<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Daftar Penyakit</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-rbsA2VBKQhggwzxH7pPCaAqO46MgnOM80zW1RWuH61DGLwZJEdK2Kadq2F9CUG65" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.8.3/font/bootstrap-icons.css">
    <link rel="stylesheet" href="//cdn.datatables.net/1.13.1/css/jquery.dataTables.min.css">
</head>
<body>
    <div class="p-4">
        <a href="index.php" type="button" class="btn btn-danger btn-sm">Kembali</a><br><br>
    <table class="table table-bordered table-striped">
        <thead>
            <tr>
                <td>No</td>
                <td>Nama Penyakit</td>
                <td>Deskripsi</td>
                <td>Solusi</td>
            </tr>
        </thead>
        <tbody>
        <?php 
        include "connection.php";

        $sql = "SELECT * FROM penyakit";
        $result = mysqli_query($mysqli, $sql);

        $i = 1;

        while ($r = mysqli_fetch_assoc($result)) { ?>
        <tr>
            <td><?php echo $i; ?></td>
            <td><?php echo $r['nama_penyakit']; ?></td>
            <td><?php echo $r['definisi']; ?></td>
            <td><?php echo $r['solusi'] ?></td>
        </tr>
        <?php
            $i += 1;
        }
        ?>
        </tbody>
    </table>
    </div>
    <script src="https://code.jquery.com/jquery-3.6.3.js" integrity="sha256-nQLuAZGRRcILA+6dMBOvcRh5Pe310sBpanc6+QBmyVM=" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/js/bootstrap.min.js" integrity="sha384-cuYeSxntonz0PPNlHhBs68uyIAVpIIOZZ5JqeqvYYIcEL727kskC66kF92t6Xl2V" crossorigin="anonymous"></script>
    <script src="//cdn.datatables.net/1.13.1/js/jquery.dataTables.min.js"></script>
    <script>
        $(document).ready( function () {
            $('#myTable').DataTable();
        } );
    </script>
</body>
</html>