<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-rbsA2VBKQhggwzxH7pPCaAqO46MgnOM80zW1RWuH61DGLwZJEdK2Kadq2F9CUG65" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/js/bootstrap.min.js"
        integrity="sha384-cuYeSxntonz0PPNlHhBs68uyIAVpIIOZZ5JqeqvYYIcEL727kskC66kF92t6Xl2V"
        crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.8.3/font/bootstrap-icons.css">
    <link rel="stylesheet" href="asset/style.css">
    <title>Hasil Konsultasi</title>
</head>

<body>

    <div id="main">
        <h2 class="text-center"><span style="font-size:30px;cursor:pointer"></span>
            HASIL KONSULTASI </h2><br>
        <?php
        require "connection.php";
        session_start();

        // $id = $_SESSION['id_penyakit'];
        $id = $_SESSION['id'];

        $sql = "SELECT * FROM penyakit where id_penyakit = $id";
        $result1 = mysqli_query($mysqli, $sql);
        $r1 = mysqli_fetch_array($result1);
        

        ?>
        <div class="conatiner-hasil">
            <div class="card">
                <div class="card-header">
                    <div class="alert alert-success text-center" role="alert">
                        Hasil Diagnosa
                    </div>
                </div>
                <div class="card-body">
                    <?php
                    $jawab = $_POST['jawab'];
                    if (preg_match("/ya tidak/", implode(" ", $jawab)) || preg_match("/tidak ya/", implode(" ", $jawab))) {
                        echo "Anda tidak menderita penyakit : " . $r1['nama_penyakit'];
                        echo "<br>Pertanyaan : ";
                        $sql2 = "SELECT p.nama_gejala,p.pertanyaan, a.id_gejala AS id FROM gejala p, aturan a WHERE p.id_gejala = a.id_gejala AND a.id_penyakit = $id";
                        $result2 = mysqli_query($mysqli, $sql2);
                        $x = 0;
                        while($r2 = mysqli_fetch_array($result2)){
                            echo "<li>". $r2['pertanyaan'] . $jawab[$x] .  "</li>";
                            $x += 1;
                        }
                    }
                    if (!preg_match("/ya/", implode(" ", $jawab))) {
                        echo "Anda tidak menderita penyakit : " . $r1['nama_penyakit'];
                        echo "<br>Pertanyaan : ";
                        $sql2 = "SELECT p.nama_gejala,p.pertanyaan, a.id_gejala AS id FROM gejala p, aturan a WHERE p.id_gejala = a.id_gejala AND a.id_penyakit = $id";
                        $result2 = mysqli_query($mysqli, $sql2);
                        $x = 0;
                        while($r2 = mysqli_fetch_array($result2)){
                            echo "<li>". $r2['pertanyaan'] . $jawab[$x] .  "</li>";
                            $x += 1;
                        }
                    }
                    if (!preg_match("/tidak/", implode(" ", $jawab))) {
                        echo "Anda menderita penyakit : " . "<i><u><b>" . $r1['nama_penyakit'] . "</b></u></i><br>" . "<br>";
                        $result2 = mysqli_query($mysqli, $sql);
                        echo "Pertanyaan : ";
                        $sql2 = "SELECT p.nama_gejala,p.pertanyaan, a.id_gejala AS id FROM gejala p, aturan a WHERE p.id_gejala = a.id_gejala AND a.id_penyakit = $id";
                        $result2 = mysqli_query($mysqli, $sql2);
                        $x = 0;
                        while($r2 = mysqli_fetch_array($result2)){
                            echo "<li>". $r2['pertanyaan'] . $jawab[$x] .  "</li>";
                            $x += 1;
                        }
                        echo  "<br><b>Definisi : </b><br>" . $r1['definisi'] . PHP_EOL;
                        echo "<br><br><b>Solusi : </b>" . PHP_EOL; 
                        ?>
                        
                    <?php
                        echo $r1['solusi'];
                    }
                    ?>
                </div>
            </div>


            <br>
            <button class="btn btn-danger btn-sm"><a href="index_konsultasi.php"
                    style="text-decoration: none; color: #ffffff;">Konsultasi
                    Lagi</a></button>
        </div>
    </div>
</body>

</html>