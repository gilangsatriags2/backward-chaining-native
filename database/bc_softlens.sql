-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 24, 2022 at 05:19 PM
-- Server version: 10.4.25-MariaDB
-- PHP Version: 8.1.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_softlens`
--

-- --------------------------------------------------------

--
-- Table structure for table `aturan`
--

CREATE TABLE `aturan` (
  `id_aturan` int(5) NOT NULL,
  `kode_aturan` varchar(5) NOT NULL,
  `id_gejala` int(5) NOT NULL,
  `id_penyakit` int(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `aturan`
--

INSERT INTO `aturan` (`id_aturan`, `kode_aturan`, `id_gejala`, `id_penyakit`) VALUES
(45, 'A001', 1, 1),
(46, 'A002', 2, 1),
(47, 'A003', 3, 1),
(48, 'A004', 3, 2),
(49, 'A005', 4, 2),
(50, 'A006', 6, 2),
(51, 'A007', 8, 2),
(52, 'A008', 9, 2),
(53, 'A009', 4, 3),
(54, 'A010', 5, 3),
(55, 'A011', 21, 3),
(56, 'A012', 10, 4),
(57, 'A013', 11, 4),
(58, 'A014', 12, 4),
(59, 'A015', 1, 5),
(60, 'A016', 2, 5),
(61, 'A017', 3, 5),
(62, 'A018', 8, 5),
(63, 'A019', 13, 5),
(64, 'A020', 14, 5),
(65, 'A021', 21, 5),
(66, 'A022', 3, 6),
(67, 'A023', 9, 6),
(68, 'A024', 12, 6),
(69, 'A025', 13, 6),
(70, 'A026', 21, 6),
(71, 'A027', 2, 7),
(72, 'A028', 13, 7),
(73, 'A029', 16, 7),
(74, 'A030', 21, 7),
(75, 'A031', 2, 8),
(76, 'A032', 12, 8),
(77, 'A033', 15, 8),
(78, 'A034', 17, 8),
(79, 'A035', 18, 8),
(80, 'A036', 21, 8),
(81, 'A037', 8, 9),
(82, 'A038', 15, 9),
(83, 'A039', 4, 10),
(84, 'A040', 5, 10),
(85, 'A041', 15, 10),
(86, 'A042', 19, 10),
(88, 'A044', 7, 2);

-- --------------------------------------------------------

--
-- Table structure for table `gejala`
--

CREATE TABLE `gejala` (
  `id_gejala` int(5) NOT NULL,
  `kode_gejala` varchar(5) NOT NULL,
  `nama_gejala` varchar(250) NOT NULL,
  `pertanyaan` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `gejala`
--

INSERT INTO `gejala` (`id_gejala`, `kode_gejala`, `nama_gejala`, `pertanyaan`) VALUES
(1, 'G001', 'Penderita merasa tidak nyaman pada mata', 'Apakah penderita merasa tidak nyaman pada mata ?'),
(2, 'G002', 'Sensitif pada cahaya / photopobia\r\n', 'Apakah mata sensitif pada cahaya / photopobia ?'),
(3, 'G003', 'Ada noda pada kornea\r\n', 'Apakah ada noda pada kornea ?'),
(4, 'G004', 'Timbul rasa gatal\r\n', 'Apakah timbul rasa gatal di sekitar mata ?'),
(5, 'G005', 'Pembengkakan kelopak mata\r\n', 'Apakah ada pembengkakan di kelopak mata ?'),
(6, 'G006', 'Kelopak mata seperti terbakar\r\n', 'Apakah kelopak mata seperti terbakar ?'),
(7, 'G007', 'Timbul kerak di sekitar kelopak mata\r\n', 'Apakah timbul kerak di sekitar kelopak mata ?'),
(8, 'G008', 'Pembuluh darah tampak jelas\r\n', 'Apakah pembuluh darah pada mata tampak jelas ?'),
(9, 'G009', 'Kelopak mata saling menempel\r\n', 'Apakah kelopak mata saling menempel ?'),
(10, 'G010', 'Mata seperti terbakar\r\n', 'Apakah mata seperti terbakar ?'),
(11, 'G011', 'Air mata sering keluar\r\n', 'Apakah air mata sering keluar ?'),
(12, 'G012', 'Cairan di mata berlebihan\r\n', 'Apakah cairan di mata berlebihan ?'),
(13, 'G013', 'Penglihatan berkabut', 'Apakah penglihatan berkabut ?'),
(14, 'G014', 'Ada krista di kornea', 'Apakah ada krista di kornea ?'),
(15, 'G015', 'Penglihatan berkurang', 'Apakah penglihatan berkurang ?'),
(16, 'G016', 'Timbul noda putih pada mata', 'Apakah timbul noda putih pada mata ?'),
(17, 'G017', 'Merasa sakit di mata', 'Apakah ada rasa sakit di mata ?'),
(18, 'G018', 'Mata bernanah', 'Apakah mata bernanah ?'),
(19, 'G019', 'Gerakan lensa berlebihan', 'Apakah gerakan lensa berlebihan ?'),
(21, 'G021', 'Mata merah', 'Apakah mata anda merah ?');

-- --------------------------------------------------------

--
-- Table structure for table `login`
--

CREATE TABLE `login` (
  `id` int(11) NOT NULL,
  `username` varchar(25) CHARACTER SET latin1 NOT NULL,
  `password` varchar(255) CHARACTER SET latin1 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `login`
--

INSERT INTO `login` (`id`, `username`, `password`) VALUES
(3, 'adm1', 'admin123'),
(4, 'usr1', 'user1');

-- --------------------------------------------------------

--
-- Table structure for table `penyakit`
--

CREATE TABLE `penyakit` (
  `id_penyakit` int(5) NOT NULL,
  `kode_penyakit` varchar(5) NOT NULL,
  `nama_penyakit` varchar(250) NOT NULL,
  `definisi` text NOT NULL,
  `solusi` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `penyakit`
--

INSERT INTO `penyakit` (`id_penyakit`, `kode_penyakit`, `nama_penyakit`, `definisi`, `solusi`) VALUES
(1, 'D001', 'Noda Kornea', 'Noda Kornea adalah peradangan pada kornea mata yang dapat disebabkan oleh reaksi alergi, infeksi, dan cedera.', '<ul>\n<li>Gunakan cairan softlens yang sesuai dengan softlens</li>\n<li>Hindari penggunaan lensa kontak karena dapat menyebabkan mata terasa perih dan semakin tidak nyaman</li>\n<li>Gunakan cairan tetes mata untuk menghilangkan kotoran pada mata</li>\n</ul>'),
(2, 'D002', 'Blepharitis\r\n', 'Blefaritis adalah peradangan di kelopak mata yang menyebabkan bagian tersebut menjadi bengkak, kemerahan, dan berminyak. Selain tidak enak dipandang, kondisi ini juga dapat membuat penderitanya merasa tidak nyaman. Meski begitu, blefaritis umumnya tidak menular.', '<ul>\r\n<li>Gunakan cairan tetes mata untuk menghilangkan kotoran pada mata</li>\r\n<li>Gunakan obat tetes antihistamin pada mata untuk mengurangi gatal karena alergi</li>\r\n<li>Kompres hangat, pembersihan pada kelopak mata, penggunaan antibiotik topikal & oral</li>\r\n<li>Hindari memakai lensa kontak sampai mata merah mulai menghilang, bersihkan lensa dengan benar dan jangan menggunakan kembali lensa yang sekali pakai, hindari menggaruk atau menggosok mata</li>\r\n<li>Gunakan obat tetes antihistamin dan hindari menggunakan lensa kontak selama 1 minggu</li>\r\n<li>Gunakan pelembab, kompres es batu\r\n<li>Guna\r\n</ul>'),
(3, 'D003', 'Reaksi Alergi\r\n', 'Alergi pada mata merupakan kondisi yang cukup umum terjadi dan diderita lebih dari jutaan orang di seluruh dunia. Seperti halnya alergi pernapasan dan kulit, alergi mata terjadi ketika sistem kekebalan tubuh menemukan adanya kuman, bakteri, atau alergen pada mata dan kemudian melawannya sehingga muncul reaksi alergi.', '<ul>\r\n<li>Gunakan obat tetes antihistamin pada mata untuk mengurangi gatal karena alergi</li>\r\n<li>Hindari mengucek mata, cuci tangan sebelum menyentuh daerah mata, lakukan penggunaan dan perawatan lensa kontak secara benar</li>\r\n<li>Gunakan obat tetes, rendam dengan air es atau dirambang, setelah itu istirahatkan mata</li>\r\n</ul>'),
(4, 'D004', 'Sindrom mata kering\r\n', 'Normalnya, air mata akan mengaliri permukaan mata ketika mata berkedip. Akan tetapi, pada mata kering, produksi atau komposisi air mata mengalami gangguan. Akibatnya, permukaan mata tidak terlumasi dengan baik. Kondisi ini menimbulkan sindrom mata kering atau keratoconjunctivitis sicca.', '<ul>\r\n<li>Gunakan obat nyeri, Antihistamin, antibiotik dan pembilasan</li>\r\n<li>Pakai kacamata hitam untuk menghalau sinar radiasi UV, hindari memegang atau bahkan mengucek mata Anda, gunakan obat tetes mata</li>\r\n<li>Lakukanlah kompres mata dengan memakai air hangat, jangan memegang-megang area sekitar mata dengan tangan kotor, jangan pula berlebihan menggunakan kosmetika di sekitar mata</li>\r\n</ul>'),
(5, 'D005', 'Corneal Edema\r\n', 'Edema kornea adalah pembengkakan kornea yang disebabkan oleh penumpukan cairan di kornea. Bila tida diobati, edema kornea dapat menyebabkan penglihatan kabur.', '<ul>\r\n<li>Gunakan cairan softlens yang sesuai dengan softlens</li>\r\n<li>Hindari penggunaan lensa kontak karena dapat menyebabkan mata terasa perih dan semakin tidak nyaman</li>\r\n<li>Gunakan cairan tetes mata untuk menghilangkan kotoran pada mata</li>\r\n<li>Hindari memakai lensa kontak sampai mata merah mulai menghilang, bersihkan lensa dengan benar dan jangan menggunakan kembali lensa yang sekali pakai, hindari menggaruk atau menggosok mata</li>\r\n<li>Hindari tidur larut malam, dan mencegah kelelahan mata Anda. Jika itu katarak maka disarankan operasi</li>\r\n<li>Berikan obat antibakteri, antijamur, atau antivirus dan obat antiradang steroid.</li>\r\n<li>Gunakan obat tetes, rendam dengan air es atau dirambang, setelah itu istirahatkan mata</li>\r\n</ul>'),
(6, 'D006', 'Infeksi Mata\r\n', 'Infeksi mata adalah penyakit yang terjadi ketika ada bakteri, jamur, parasit, atau virus yang menginfeksi mata. Infeksi dapat menyerang salah satu atau kedua mata.', '<ul>\r\n<li>Gunakan cairan tetes mata untuk menghilangkan kotoran pada mata</li>\r\n<li>Gunakan obat tetes antihistamin dan hindari menggunakan lensa kontak selama 1 minggu</li>\r\n<li>Lakukanlah kompres mata dengan memakai air hangat, jangan memegang-megang area sekitar mata dengan tangan kotor, jangan pula berlebihan menggunakan kosmetika di sekitar mata</li>\r\n<li>Hindari tidur larut malam, dan mencegah kelelahan mata Anda. Jika itu katarak maka disarankan operasi</li>\r\n<li>Gunakan obat tetes, rendam dengan air es atau dirambang, setelah itu istirahatkan mata</li>\r\n</ul>'),
(7, 'D007', 'Infitrates\r\n', 'Infiltrasi mata adalah bocornya cairan atau obat-obatan ke jaringan mata, yang dapat menyebabkan pembengkakan.', '<ul>\r\n<li>Hindari penggunaan lensa kontak karena dapat menyebabkan mata terasa perih dan semakin tidak nyaman</li>\r\n<li>Hindari tidur larut malam, dan mencegah kelelahan mata Anda. Jika itu katarak maka disarankan operasi</li>\r\n<li>Gunakan kacamata hitam untuk mengurangi cahata ultraviolet ke mata, gunakan obat tetes untuk meredakan iritasi ringan atau melumasi mata yang kering</li>\r\n<li>Gunakan obat tetes, rendam dengan air es atau dirambang, setelah itu istirahatkan mata</li>\r\n</ul>'),
(8, 'D008', 'Mocrobila Keratitis\r\n', 'Keratitis adalah peradangan pada kornea mata. Kondisi ini umumnya ditandai dengan mata merah yang disertai nyeri. Penyebab keratitis bervariasi, mulai dari cedera hingga infeksi.', '<ul>\r\n<li>Hindari penggunaan lensa kontak karena dapat menyebabkan mata terasa perih dan semakin tidak nyaman</li>\r\n<li>Lakukanlah kompres mata dengan memakai air hangat, jangan memegang-megang area sekitar mata dengan tangan kotor, jangan pula berlebihan menggunakan kosmetika di sekitar mata</li>\r\n<li>Lakukan olahraga yang teratur, menggunakan lensa khusus serta menjaga pola makan yang baik</li>\r\n<li>Gunakan obat tetes mata atau timun yang direndam di air dingin selama 10 menit kemudian di taruh di atas mata yang tertutup</li>\r\n<li>Gunakan obat tetes mata antibiotik dan hindari infeksi mata yang menyebabkan mata bernanah, hilangkan kebiasaan menyentuh, menggosok, atau menggaruk mata dan area di sekitarnya tanpa mencuci tangan terlebih dahulu.</li>\r\n<li>Gunakan obat tetes, rendam dengan air es atau dirambang, setelah itu istirahatkan mata</li>\r\n</ul>'),
(9, 'D009', 'Vaskularisasi Kornea\r\n', 'Vaskularisasi kornea adalah pembentukan dan pertumbuhan pembuluh darah, yang menyebabkan munculnya berbagai penyakit', '<ul>\r\n<li>Hindari memakai lensa kontak sampai mata merah mulai menghilang, bersihkan lensa dengan benar dan jangan menggunakan kembali lensa yang sekali pakai, hindari menggaruk atau menggosok mata</li>\r\n<li>Lakukan olahraga yang teratur, menggunakan lensa khusus serta menjaga pola makan yang baik</li>\r\n</ul>'),
(10, 'D010', 'Giant Papilary Conjunctivitas', 'Giant papillary conjunctivitis (GPC) adalah reaksi alergi pada mata. GPC terjadi ketika satu atau beberapa benjolan bundar kecil (papillae) terbentuk di bagian bawah kelopak mata.', '<ul>\r\n<li>Gunakan obat tetes antihistamin pada mata untuk mengurangi gatal karena alergi</li>\r\n<li>Hindari mengucek mata, cuci tangan sebelum menyentuh daerah mata, lakukan penggunaan dan perawatan lensa kontak secara benar</li>\r\n<li>Lakukan olahraga yang teratur, menggunakan lensa khusus serta menjaga pola makan yang baik</li>\r\n<li>Istirahatkan mata, rendam dengan air es</li>\r\n</ul>');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `aturan`
--
ALTER TABLE `aturan`
  ADD PRIMARY KEY (`id_aturan`),
  ADD KEY `id_gejala` (`id_gejala`),
  ADD KEY `id_penyakit` (`id_penyakit`);

--
-- Indexes for table `gejala`
--
ALTER TABLE `gejala`
  ADD PRIMARY KEY (`id_gejala`);

--
-- Indexes for table `login`
--
ALTER TABLE `login`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `penyakit`
--
ALTER TABLE `penyakit`
  ADD PRIMARY KEY (`id_penyakit`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `aturan`
--
ALTER TABLE `aturan`
  MODIFY `id_aturan` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=92;

--
-- AUTO_INCREMENT for table `gejala`
--
ALTER TABLE `gejala`
  MODIFY `id_gejala` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=47;

--
-- AUTO_INCREMENT for table `login`
--
ALTER TABLE `login`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `penyakit`
--
ALTER TABLE `penyakit`
  MODIFY `id_penyakit` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `aturan`
--
ALTER TABLE `aturan`
  ADD CONSTRAINT `aturan_ibfk_1` FOREIGN KEY (`id_gejala`) REFERENCES `gejala` (`id_gejala`),
  ADD CONSTRAINT `aturan_ibfk_2` FOREIGN KEY (`id_penyakit`) REFERENCES `penyakit` (`id_penyakit`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
